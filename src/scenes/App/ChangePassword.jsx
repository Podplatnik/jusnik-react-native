import React, {useContext, useState} from "react";
import * as yup from "yup";
import {AppContext} from "../../store/AppContextProvider";
import {theme} from "../../constants";
import Block from "../../components/Block";
import Text from "../../components/Text";
import {Formik} from "formik";
import {Toast} from "native-base";
import Input from "../../components/Input";
import {globalStyles} from "../../styles/global";
import {ActivityIndicator, ScrollView} from "react-native";
import Button from "../../components/Button";

const ChangePasswordSchema = yup.object({
    oldPassword: yup.string().required().min(8).max(20),
    newPassword: yup.string().required().min(8).max(20),
    confirmNewPassword: yup.string().required().min(8).max(20)
        .oneOf([yup.ref('newPassword'), null], 'Passwords must match.'),
})

const ChangePassword = (props) => {
    const {logout, changePassword} = useContext(AppContext);
    const [loading, setLoading] = useState(false);
    return (
        <ScrollView>
            <Block padding={[0, theme.sizes.base * 2]}>
                <Text h2 bold>Change Your Password</Text>
                <Text>
                    If you'd like to change your password, enter your current
                    and new passwords here. After a successful change you WILL be
                    asked to login again with the new password.
                </Text>
                <Formik
                    initialValues={{
                        oldPassword: '',
                        newPassword: '',
                        confirmNewPassword: '',
                    }}
                    onSubmit={async (values) => {
                        setLoading(true)
                        try {
                            await changePassword(values)
                            Toast.show({
                                text: "Update successful. Logging you out...",
                                type: "success",
                                duration: 2000
                            });
                            setTimeout(async () => {
                                logout()
                            }, 2000)
                        } catch (e) {
                            if (e.response) {
                                const message = e.response.data.message || 'Can not change password.'
                                await Toast.show({
                                    text: message,
                                    duration: 2000,
                                    buttonText: 'Unlucky'
                                });
                            } else {
                                await Toast.show({
                                    text: 'Network Error',
                                    buttonText: 'Dismiss'
                                })
                            }
                        } finally {
                            setLoading(false);
                        }
                    }}
                    validationSchema={ChangePasswordSchema}>
                    {(props) => (
                        <Block>
                            <Input
                                secure
                                label="Current Password"
                                style={globalStyles.input}
                                onChangeText={props.handleChange('oldPassword')}
                                value={props.values.oldPassword}
                                onBlur={props.handleBlur('oldPassword')}/>
                            <Text style={globalStyles.errorText}>
                                {props.touched.oldPassword && props.errors.oldPassword}
                            </Text>
                            <Input
                                secure
                                label="New Password"
                                style={globalStyles.input}
                                onChangeText={props.handleChange('newPassword')}
                                value={props.values.newPassword}
                                onBlur={props.handleBlur('newPassword')}/>
                            <Text style={globalStyles.errorText}>
                                {props.touched.newPassword && props.errors.newPassword}
                            </Text>
                            <Input
                                secure
                                label="Confirm New Password"
                                style={globalStyles.input}
                                onChangeText={props.handleChange('confirmNewPassword')}
                                value={props.values.confirmNewPassword}
                                onBlur={props.handleBlur('confirmNewPassword')}/>
                            <Text style={globalStyles.errorText}>
                                {props.touched.confirmNewPassword && props.errors.confirmNewPassword}
                            </Text>
                            <Button gradient onPress={() => props.handleSubmit()}>
                                {loading ? (
                                    <ActivityIndicator size="small" color="white"/>
                                ) : (
                                    <Text bold white center>
                                        Change
                                    </Text>
                                )}
                            </Button>
                        </Block>
                    )}
                </Formik>
            </Block>
        </ScrollView>
    )
}

export default ChangePassword;
