import React, {useReducer} from 'react';
import {AsyncStorage} from "react-native";
import {defaultReducer} from "./reducers/defaultReducer";
import {deleteMe, retrieveMe, updateMe, updatePassword} from "../api/user";
import request from "../utils/request";
import {tokenExchange, signUp as signUpApi} from "../api/auth";

export const AppContext = React.createContext();
export const INITIAL_STATE = {
    user: null,
    token: null
}

const AppContextProvider = (props) => {
    const [state, dispatch] = useReducer(defaultReducer, INITIAL_STATE);
    const attemptAutoLogin = async () => {
        try {
            const tokenInStorage = await AsyncStorage.getItem('token');
            if (tokenInStorage) {
                request.defaults.headers.common['Authorization'] = `Bearer ${tokenInStorage}`;
                dispatch({type: 'SET_TOKEN', token: tokenInStorage});
                await fetchUser();
            }
        } catch (error) {
            await logout();
        }
    }
    const fetchUser = async () => {
        const {data} = await retrieveMe();
        await AsyncStorage.setItem('user', JSON.stringify(data));
        dispatch({type: 'SET_USER', user: data});
    }

    const login = async (email, password) => {
        const response = await tokenExchange({email, password});
        dispatch({type: 'SET_TOKEN', token: response.data.token});
        await AsyncStorage.setItem('token', response.data.token);
        request.defaults.headers.common['Authorization'] = `Bearer ${response.data.token}`;
        await fetchUser();
    }

    const logout = async () => {
        await AsyncStorage.removeItem('token');
        await AsyncStorage.removeItem('user');
        delete request.defaults.headers.common['Authorization'];
        dispatch({type: 'SET_TOKEN', token: null});
        dispatch({type: 'SET_USER', user: null});
    }

    const signUp = async (data) => {
        await signUpApi(data);
    }

    const updateUser = async (data) => {
        const response = await updateMe(data);
        dispatch({type: 'SET_USER', user: response.data});
    }

    const changePassword = async (data) => {
        await updatePassword(data);
    }

    const deleteUser = async () => {
        await deleteMe();
        await logout();
    }

    return (
        <AppContext.Provider value={{
            state,
            attemptAutoLogin,
            login,
            logout,
            signUp,
            updateUser,
            changePassword,
            deleteUser
        }}>
            {props.children}
        </AppContext.Provider>
    )
}

export default AppContextProvider;
