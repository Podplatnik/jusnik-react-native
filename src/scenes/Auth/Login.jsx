import React, {useState, useContext} from "react";
import Block from "../../components/Block";
import Text from "../../components/Text";
import Button from "../../components/Button";
import {theme} from "../../constants";
import Input from "../../components/Input";
import {StyleSheet, ScrollView, Image, View, ActivityIndicator, Keyboard} from "react-native";
import {globalStyles} from "../../styles/global";
import {AppContext} from "../../store/AppContextProvider";
import {Toast} from 'native-base';
import * as yup from 'yup';
import {Formik} from "formik";

const LoginSchema = yup.object({
    email: yup.string().required().email(),
    password: yup.string().required().min(8).max(20),
})

export function Login({navigation}) {
    const [loading, setLoading] = useState(false);
    const {login} = useContext(AppContext);

    return (
        <ScrollView>
            <Block padding={[theme.sizes.base * 3, theme.sizes.base * 2]}>
                <View style={styles.center}>
                    <Image style={globalStyles.logo} source={require('../../assets/logo.png')}/>
                </View>
                <Text h2 bold>Login</Text>
                <Formik
                    initialValues={{
                        email: '',
                        password: '',
                    }}
                    onSubmit={async ({email, password}) => {
                        setLoading(true);
                        Keyboard.dismiss()

                        try {
                            await login(email, password);
                        } catch (e) {
                            if (e.response) {
                                console.log(e.response.data)
                                const message = e.response.data?.message || "Can't login."
                                await Toast.show({
                                    text: message,
                                    buttonText: 'Unlucky'
                                });
                            } else {
                                await Toast.show({
                                    text: 'Network Error',
                                    buttonText: 'Dismiss'
                                })
                            }
                            setLoading(false);
                        }
                    }}
                    validationSchema={LoginSchema}>
                    {(props) => (
                        <Block middle>
                            <Input
                                label="Email"
                                style={globalStyles.input}
                                onChangeText={props.handleChange('email')}
                                value={props.values.email}
                                onBlur={props.handleBlur('email')}/>
                            <Text style={globalStyles.errorText}>
                                {props.touched.email && props.errors.email}
                            </Text>

                            <Input
                                secure
                                label="Password"
                                style={globalStyles.input}
                                onChangeText={props.handleChange('password')}
                                value={props.values.password}
                                onBlur={props.handleBlur('password')}/>
                            <Text style={globalStyles.errorText}>
                                {props.touched.password && props.errors.password}
                            </Text>
                            <Button gradient onPress={() => props.handleSubmit()}>
                                {loading ? (
                                    <ActivityIndicator size="small" color="white"/>
                                ) : (
                                    <Text bold white center>
                                        Login
                                    </Text>
                                )}
                            </Button>
                            <Button shadow onPress={() => navigation.navigate("SignUp")}>
                                <Text center semibold>
                                    Don't have an account ? Sign up here
                                </Text>
                            </Button>
                        </Block>
                    )}
                </Formik>
            </Block>
        </ScrollView>
    )
}

export const styles = StyleSheet.create({
    login: {
        flex: 1,
        justifyContent: "center"
    },
    hasErrors: {
        borderBottomColor: theme.colors.accent
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: -30
    }
});
