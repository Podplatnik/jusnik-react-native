import { StyleSheet } from 'react-native';
import {theme} from "../constants";

export const globalStyles = StyleSheet.create({
    logo: {
        marginBottom: 50
    },
    input: {
        borderRadius: 0,
        borderWidth: 0,
        borderBottomColor: theme.colors.gray2,
        borderBottomWidth: StyleSheet.hairlineWidth
    },
    errorText: {
        color: '#F3534A',
        textAlign: 'center'
    },
    headerRightMargin: {
        marginRight: 16
    }
});
