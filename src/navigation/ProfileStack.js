import React, {useContext} from "react";
import {createStackNavigator} from "@react-navigation/stack";
import {Profile} from "../scenes/App/Profile";
import EditProfile from "../scenes/App/EditProfile";
import ChangePassword from "../scenes/App/ChangePassword";
import {FontAwesome5} from '@expo/vector-icons';
import {View, TouchableOpacity, Alert} from "react-native";
import * as theme from "../constants/theme";
import {globalStyles} from "../styles/global";
import {AppContext} from "../store/AppContextProvider";

const Stack = createStackNavigator();

const ProfileStack = (props) => {
    const {logout} = useContext(AppContext);

    const LogoutConfirmationAlert = () => {
        Alert.alert(
            "Logout confirmation",
            "Are you sure, you want to log out ?",
            [
                {
                    text: "Take be back",
                    onPress: () => {
                    },
                    style: "cancel",
                },
                {
                    text: "Log me out",
                    onPress: () => logout(),
                }
            ],
            {cancelable: true}
        )
    }

    return (
        <Stack.Navigator screenOptions={{
            headerStyle: {
                backgroundColor: 'transparent',
                elevation: 0
            },
            title: '',
        }}>
            <Stack.Screen name='Profile' component={Profile} options={{
                headerRight: () => (
                    <View>
                        <TouchableOpacity onPress={() => LogoutConfirmationAlert()}>
                            <FontAwesome5 name='sign-out-alt' size={20} color={theme.colors.black}
                                          style={globalStyles.headerRightMargin}/>
                        </TouchableOpacity>
                    </View>
                )
            }}/>
            <Stack.Screen name='EditProfile' component={EditProfile}/>
            <Stack.Screen name='ChangePassword' component={ChangePassword}/>
        </Stack.Navigator>
    )
}

export default ProfileStack;
