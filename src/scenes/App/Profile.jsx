import React, {useContext} from "react";
import {View} from "native-base";
import {StyleSheet, Dimensions, Image, TouchableOpacity} from 'react-native';
import {AppContext} from "../../store/AppContextProvider";
import Block from "../../components/Block";
import Text from "../../components/Text";
import * as theme from "../../constants/theme";
import {FontAwesome, FontAwesome5} from '@expo/vector-icons';
import Card from "../../components/Card";
import Badge from "../../components/Badge";

const {width} = Dimensions.get("window");

export function Profile({navigation}) {
    const {state} = useContext(AppContext);
    const categories = [
        {
            id: 'editProfile',
            name: 'Edit Profile',
            icon: 'edit',
            navTo: 'EditProfile'
        },
        {
            id: 'passwordChange',
            name: 'Password',
            icon: 'key',
            navTo: 'ChangePassword'
        }
    ]

    return (
        <View>
            <Block padding={[theme.sizes.base, 0]}>
                <Block center space='between' style={styles.header}>
                    <Image source={require('../../assets/fake_avatar_large.png')}/>
                    <Text h1 bold>
                        {state.user.firstName} {state.user.lastName}
                    </Text>
                    <Text h4 gray3>
                        {state.user.email}
                    </Text>
                    <Block flex={false} row center space={'between'} style={styles.ratingContainer}>
                        <Text h4 gray3>
                            4.2 / 5
                        </Text>
                        <Text style={styles.ratingIcon}>
                            <FontAwesome name='star' size={16}/>
                        </Text>
                    </Block>
                </Block>
                <Block flex={false} row space="between" style={styles.categories}>
                    {categories.map(category => (
                        <TouchableOpacity
                            key={category.name}
                            onPress={() => navigation.navigate(category.navTo)}
                        >
                            <Card center middle shadow style={styles.category}>
                                <Badge
                                    margin={[0, 0, 15]}
                                    size={50}
                                    color="#2BDA8E"
                                >
                                    <FontAwesome5 name={category.icon} size={16} color='white'/>
                                </Badge>
                                <Text medium height={20}>
                                    {category.name}
                                </Text>
                            </Card>
                        </TouchableOpacity>
                    ))}
                </Block>
            </Block>
        </View>
    )
}

const styles = StyleSheet.create({
    ratingContainer: {
        margin: 12
    },
    ratingIcon: {
        color: '#FFE358',
        marginLeft: 5
    },
    avatar: {
        height: theme.sizes.base * 2.2,
        width: theme.sizes.base * 2.2
    },
    categories: {
        flexWrap: "wrap",
        paddingHorizontal: theme.sizes.base * 1.5,
        marginBottom: theme.sizes.base * 3.5,
        paddingVertical: theme.sizes.padding * 10,
    },
    category: {
        minWidth: (width - theme.sizes.padding * 2.4 - theme.sizes.base) / 2,
        maxWidth: (width - theme.sizes.padding * 2.4 - theme.sizes.base) / 2,
        maxHeight: (width - theme.sizes.padding * 2.4 - theme.sizes.base) / 2,
        minHeight: (width - theme.sizes.padding * 2.4 - theme.sizes.base) / 2
    }
});
