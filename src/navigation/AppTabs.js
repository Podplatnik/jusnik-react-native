import React from "react";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {Profile} from "../scenes/App/Profile";
import {Reservations} from "../scenes/App/Reservations";
import {Restaurants} from "../scenes/App/Restaurants";
import {FontAwesome} from '@expo/vector-icons';
import ProfileStack from "./ProfileStack";

const Tabs = createBottomTabNavigator();

const AppTabs = (props) => {
    return (
        <Tabs.Navigator
            initialRouteName='Profile'
            tabBarOptions={{
                activeTintColor: '#17ac6c',
                inactiveTintColor: 'gray',
            }}
            screenOptions={({route}) => ({
                tabBarIcon: ({focused, color, size}) => {
                    let iconName;
                    if (route.name === 'Profile') {
                        iconName = focused ? 'user' : 'user-o'
                    } else if (route.name === 'Reservations') {
                        iconName = focused ? 'tags' : 'tag'
                    } else if (route.name === 'Restaurants') {
                        iconName = focused ? 'map' : 'map-o'
                    }

                    return <FontAwesome name={iconName} size={size} color={color}/>;
                },
            })}>
            <Tabs.Screen name='Restaurants' component={Restaurants}/>
            <Tabs.Screen name='Reservations' component={Reservations}/>
            <Tabs.Screen name='Profile' component={ProfileStack}/>
        </Tabs.Navigator>
    )
}

export default AppTabs;
