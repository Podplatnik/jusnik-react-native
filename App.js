import React from 'react';
import AppContextProvider from "./src/store/AppContextProvider";
import {Routes} from "./src/navigation/Routes";
import {Root} from "native-base";

export default function App() {
    return (
        <AppContextProvider>
            <Root>
                <Routes/>
            </Root>
        </AppContextProvider>
    );
}
