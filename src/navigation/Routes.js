import React, {useContext, useEffect, useState} from 'react';
import {NavigationContainer} from "@react-navigation/native";
import {AppContext} from "../store/AppContextProvider";
import AuthStack from './AuthStack';
import AppTabs from './AppTabs';
import * as Font from "expo-font";
import {Center} from "../components/Center";
import {Spinner} from "native-base";

export const Routes = (props) => {
    const {state, attemptAutoLogin} = useContext(AppContext);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        Font.loadAsync({
            Roboto: require('native-base/Fonts/Roboto.ttf'),
            Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
        });
        attemptAutoLogin().finally(() => setLoading(false));
    }, [])

    if (loading) {
        return (
            <Center>
                <Spinner/>
            </Center>
        )
    }

    return (
        <NavigationContainer>
            {state.user ? <AppTabs/> : <AuthStack/>}
        </NavigationContainer>
    )
}
