import React, {useState, useEffect, useContext} from "react";
import {theme} from "../../constants";
import Block from "../../components/Block";
import Text from "../../components/Text";
import {Formik} from "formik";
import Input from "../../components/Input";
import {globalStyles} from "../../styles/global";
import Button from "../../components/Button";
import {ActivityIndicator, Keyboard, ScrollView} from 'react-native';
import * as yup from "yup";
import {Toast} from "native-base";
import {AppContext} from "../../store/AppContextProvider";

const RegisterSchema = yup.object({
    email: yup.string().required().email(),
    firstName: yup.string().required().min(2).max(20),
    lastName: yup.string().required().min(2).max(30),
    password: yup.string().required().min(8).max(20),
})

export function SignUp({navigation}) {
    useEffect(() => {
        Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
        Keyboard.addListener("keyboardDidHide", _keyboardDidHide);

        // cleanup function
        return () => {
            Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
            Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
        };
    }, []);

    const _keyboardDidShow = () => {
        setShowTitle(false);
    };

    const _keyboardDidHide = () => {
        setShowTitle(true)
    };

    const [loading, setLoading] = useState(false);
    const [showTitle, setShowTitle] = useState(true);
    const {signUp} = useContext(AppContext);
    return (
        <ScrollView>
            <Block padding={[0, theme.sizes.base * 2]}>
                {showTitle ? (
                    <Text h2 bold>Registration</Text>
                ) : null}
                <Formik
                    initialValues={{
                        email: '',
                        firstName: '',
                        lastName: '',
                        password: '',
                    }}
                    onSubmit={async (values) => {
                        setLoading(true);
                        Keyboard.dismiss()

                        try {
                            await signUp(values);
                            Toast.show({
                                text: "Sign up successful. Redirecting...",
                                type: "success"
                            });
                            setTimeout(async () => {
                                navigation.navigate('Login');
                            }, 1501)
                        } catch (e) {
                            if (e.response) {
                                const message = e.response.data.message || 'Can not register. Check your fields.'
                                await Toast.show({
                                    text: message,
                                    duration: 2000,
                                    buttonText: 'Unlucky'
                                });
                            } else {
                                await Toast.show({
                                    text: 'Network Error',
                                    buttonText: 'Dismiss'
                                })
                            }
                        } finally {
                            setLoading(false);
                        }
                    }}
                    validationSchema={RegisterSchema}
                >
                    {(props) => (
                        <Block middle>
                            <Input
                                label="Email"
                                style={globalStyles.input}
                                onChangeText={props.handleChange('email')}
                                value={props.values.email}
                                onBlur={props.handleBlur('email')}/>
                            <Text style={globalStyles.errorText}>
                                {props.touched.email && props.errors.email}
                            </Text>
                            <Input
                                label="First Name"
                                style={globalStyles.input}
                                onChangeText={props.handleChange('firstName')}
                                value={props.values.firstName}
                                onBlur={props.handleBlur('firstName')}/>
                            <Text style={globalStyles.errorText}>
                                {props.touched.firstName && props.errors.firstName}
                            </Text>
                            <Input
                                label="Last Name"
                                style={globalStyles.input}
                                onChangeText={props.handleChange('lastName')}
                                value={props.values.lastName}
                                onBlur={props.handleBlur('lastName')}/>
                            <Text style={globalStyles.errorText}>
                                {props.touched.lastName && props.errors.lastName}
                            </Text>
                            <Input
                                secure
                                label="Password"
                                style={globalStyles.input}
                                onChangeText={props.handleChange('password')}
                                value={props.values.password}
                                onBlur={props.handleBlur('password')}/>
                            <Text style={globalStyles.errorText}>
                                {props.touched.password && props.errors.password}
                            </Text>
                            <Button gradient onPress={() => props.handleSubmit()}>
                                {loading ? (
                                    <ActivityIndicator size="small" color="white"/>
                                ) : (
                                    <Text bold white center>
                                        Sign Up
                                    </Text>
                                )}
                            </Button>
                        </Block>
                    )}
                </Formik>
            </Block>
        </ScrollView>
    )
}
