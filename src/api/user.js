import request from '../utils/request';

export function listUsers() {
    return request({
        url: '/api/users',
        method: 'get',
    });
}

export function retrieveMe() {
    return request({
        url: '/api/users/me',
        method: 'get',
    });
}

export function updateMe(data) {
    return request({
        url: '/api/users/me',
        method: 'put',
        data
    });
}

export function deleteMe() {
    return request({
        url: '/api/users/me',
        method: 'delete',
    });
}

export function updatePassword(data) {
    return request({
        url: '/api/users/me/password/change',
        method: 'post',
        data
    });
}


