import React, {useContext, useState} from "react";
import Block from "../../components/Block";
import {theme} from "../../constants";
import Text from "../../components/Text";
import {AppContext} from "../../store/AppContextProvider";
import * as yup from "yup";
import Input from "../../components/Input";
import {globalStyles} from "../../styles/global";
import {Formik} from "formik";
import {ActivityIndicator, Keyboard, StyleSheet, View, ScrollView, Alert} from "react-native";
import Button from "../../components/Button";
import Switch from "../../components/Switch";
import {Toast} from "native-base";
import Divider from "../../components/Divider";

const EditProfileSchema = yup.object({
    firstName: yup.string().required().min(2).max(20),
    lastName: yup.string().required().min(2).max(30),
    notifications: yup.boolean().required(),
})

const EditProfile = (props) => {
    const {state, updateUser, deleteUser} = useContext(AppContext);
    const [loading, setLoading] = useState(false);

    async function handleDeleteUser() {
        Alert.alert(
            "Account deletion",
            "Are you sure you want to delete your account ? This action is irreversible.",
            [
                {
                    text: "Take be back",
                    onPress: () => {
                    },
                    style: "default",
                },
                {
                    text: "Delete Account",
                    onPress: async () => {
                        try {
                            setLoading(false);
                            await deleteUser();
                        } catch (e) {
                            Toast.show({
                                text: "Can't delete account...",
                            });
                            setLoading(true);
                        }
                    },
                    style: "destructive"
                }
            ],
            {cancelable: true}
        )
    }

    return (
        <ScrollView>
            <Block padding={[0, theme.sizes.base * 2]}>
                <Text h2 bold>Edit Profile</Text>
                <Formik
                    initialValues={{
                        firstName: state.user.firstName,
                        lastName: state.user.lastName,
                        notifications: state.user.notifications,
                    }}
                    onSubmit={async (values) => {
                        setLoading(true);
                        try {
                            await updateUser(values);
                            Toast.show({
                                text: "Update successful",
                                type: "success"
                            });
                        } catch (e) {
                            if (e.response) {
                                const message = e.response.data.message || 'Can not update fields.'
                                await Toast.show({
                                    text: message,
                                    duration: 2000,
                                    buttonText: 'Unlucky'
                                });
                            } else {
                                await Toast.show({
                                    text: 'Network Error',
                                    buttonText: 'Dismiss'
                                })
                            }
                        } finally {
                            setLoading(false)
                        }
                    }}
                    validationSchema={EditProfileSchema}>
                    {(props) => (
                        <Block>
                            <Input
                                label="First Name"
                                style={globalStyles.input}
                                onChangeText={props.handleChange('firstName')}
                                value={props.values.firstName}
                                onBlur={props.handleBlur('firstName')}/>
                            <Text style={globalStyles.errorText}>
                                {props.touched.firstName && props.errors.firstName}
                            </Text>
                            <Input
                                label="Last Name"
                                style={globalStyles.input}
                                onChangeText={props.handleChange('lastName')}
                                value={props.values.lastName}
                                onBlur={props.handleBlur('lastName')}/>
                            <Text style={globalStyles.errorText}>
                                {props.touched.lastName && props.errors.lastName}
                            </Text>
                            <Block
                                row
                                center
                                space="between">
                                <Text gray1>Notifications</Text>
                                <Switch
                                    label='Receive Notifications'
                                    value={props.values.notifications}
                                    onValueChange={value => props.setFieldValue('notifications', value)}
                                />
                            </Block>
                            <Divider/>
                            <Button gradient onPress={() => props.handleSubmit()}>
                                {loading ? (
                                    <ActivityIndicator size="small" color="white"/>
                                ) : (
                                    <Text bold white center>
                                        Save Changes
                                    </Text>
                                )}
                            </Button>

                        </Block>
                    )}
                </Formik>
                <Button onPress={() => handleDeleteUser()}>
                    {loading ? (
                        <ActivityIndicator size="small" color="white"/>
                    ) : (
                        <Text bold center>
                            Delete your profile
                        </Text>
                    )}
                </Button>
            </Block>
        </ScrollView>
    )
}

export default EditProfile;
