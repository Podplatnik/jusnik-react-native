import React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import {Login} from "../scenes/Auth/Login.jsx";
import {SignUp} from "../scenes/Auth/SignUp";

const Stack = createStackNavigator();

const AuthStack = (props) => {
    return (
        <Stack.Navigator>
            <Stack.Screen name='Login' component={Login} options={{
                header: () => null
            }}/>
            <Stack.Screen name='SignUp' component={SignUp} options={{
                headerStyle: {
                    backgroundColor: 'transparent',
                    elevation: 0
                },
                title: ''
            }}/>
        </Stack.Navigator>
    )
}

export default AuthStack;
