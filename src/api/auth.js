import request from '../utils/request';

export function signUp(data) {
    return request({
        url: '/api/auth/sign-up',
        method: 'post',
        data
    })
}

export function tokenExchange(data) {
    return request({
        url: '/api/auth/token/',
        method: 'post',
        data
    })
}
