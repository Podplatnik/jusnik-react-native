import Constants from 'expo-constants';

const ENV = {
    dev: {
        apiUrl: 'http://10.0.2.2:3333' // aka localhost:3333 na AVD
    }
}

function getEnvVars(env = "") {
    if (env === null || env === undefined || env === "") return ENV.dev;
}

const env = getEnvVars(Constants.manifest.releaseChannel);
export default env;
